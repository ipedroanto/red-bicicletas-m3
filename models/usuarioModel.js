var mongoose = require('mongoose');
var Reserva = require('./reservaModel');
const uniqueValidator = require('mongoose-unique-validator');
const bcrypt = require('bcrypt');
const crypto = require('crypto');

var Token = require('./tokenModel');
var Mailer = require('../mailer/mailer');

var Schema = mongoose.Schema;

const saltRounds = 10;

const validateEmail = function(email) {
  const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/;
  return re.test(email);
};

var usuarioSchema = new Schema({
  nombre: {
    type: String,
    trim: true,
    required: [true, 'El nombre es obligatorio']
  },
  email: {
    type: String,
    trim: true,
    required: [true, 'El email es obligatorio'],
    lowercase: true,
    unique: true,
    validate: [validateEmail, 'Por favor, ingrese un Email valido'],
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,4})+$/]
  }, 
  password: {
    type: String,
    required: [true, 'El password es obligatorio']
  }, 
  passwordResetToken: String,
  passwordResetTokenExpires: Date, 
  verificado: {
    type: Boolean,
    default: false
  }
});

usuarioSchema.plugin(uniqueValidator, { message: 'El {PATH} ya existe con otro usuario' });

usuarioSchema.pre('save', function(next) {
  if (this.isModified('password')) {
    this.password = bcrypt.hashSync(this.password, saltRounds);
  }
  next();
});

usuarioSchema.methods.validPassword = function(password) {
  return bcrypt.compareSync(password, this.password);
};

usuarioSchema.methods.reservar = function(biciId, desde, hasta, cb) {
  var reserva = new Reserva({usuario: this.id, bicicleta: biciId, desde: desde, hasta: hasta});
  reserva.save(cb);
};

usuarioSchema.statics.createInstance = function(nombre, email, password, confirm_password) {
  return new this({
    nombre: nombre,
    email: email, 
    password: password,
    confirm_password: confirm_password
  });
};

usuarioSchema.statics.findUser = function(id, cb) {
  return this.findById(id, cb);
};

usuarioSchema.statics.allUsers = function(cb) {
  return this.find({}, cb).sort({nombre:1});
};

usuarioSchema.statics.addUser = function(newUser, cb) {
  this.create(newUser, cb);
};

usuarioSchema.statics.updateUser = function(id, update_values, cb) { // id is equal to code in my model
  return this.findByIdAndUpdate(id, update_values, cb);
};

usuarioSchema.statics.deleteUser = function(id, cb) {
  return this.findByIdAndDelete(id, cb);
};

usuarioSchema.methods.enviar_email_bienvenida = function(cb) {
  const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save(function(err) {
    if (err) return console.log(err.message);

    const mailOptions = {
      from: 'no-reply@redbiciceltas.com',
      to: email_destination,
      subject: 'Verficación de cuenta - Red Bicicletas',
      text: 'Hola,\n\nPor favor, para verificar su cuenta haga click en este link:\nhttp://localhost:5000\/token/confirmation\/' + token.token + '.\n', 
      html: '<p>Hola,<br><br>Por favor, para verificar su cuenta haga click en este link:<br><a href="http://localhost:5000\/token/confirmation\/' + token.token + '">http://localhost:5000\/token/confirmation\/' + token.token + '</a><br></p>'
    };

    Mailer.sendMail(mailOptions, function(err) {
      if (err) return console.log(err.message);

      console.log('Un correo de verificación fue enviado a ' + email_destination + '.');
    });
  });
};

usuarioSchema.methods.reset_password = function(cb) {
  const token = new Token({_userId: this.id, token: crypto.randomBytes(16).toString('hex')});
  const email_destination = this.email;
  token.save(function(err) {
    if (err) return cb(err);
    
    const mailOptions = {
      from: 'no-reply@redbiciceltas.com',
      to: email_destination,
      subject: 'Reseteo de Password de cuenta ',
      text: 'Hola,\n\nPor favor, para resetear el Password su cuenta haga click en este link:\nhttp://localhost:5000\/reset-password\/' + token.token + '.\n', 
      html: '<p>Hola,<br><br>Por favor, para resetear el <strong>Password</strong> su cuenta haga click en este link:<br><a href="http://localhost:5000\/reset-password\/' + token.token + '">http://localhost:5000\/token/confirmation\/' + token.token + '</a><br></p>'
    };

    Mailer.sendMail(mailOptions, function(err) {
      if (err) return console.log(err.message);

      console.log('Se envío un email para resetear el password a: ' + email_destination + '.');
    });

    cb(null);
  });
};

module.exports = mongoose.model('Usuario', usuarioSchema);

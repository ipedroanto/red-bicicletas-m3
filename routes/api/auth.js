const express = require('express');
const router = express.Router();

const authController = require('../../controllers/api/authApiController');

router.post('/authenticate', authController.authenticate);
router.post('/forgot-password', authController.forgot_password);

module.exports = router;

var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bicicletasRouter = require('./routes/bicicletas');
var bicicletasApiRouter = require('./routes/api/bicicletas');
var usuariosApiRouter = require('./routes/api/usuarios');
var usuariosRouter = require('./routes/usuarios');
var tokenRouter = require('./routes/token');
var authApiRouter = require('./routes/api/auth');

const passport = require('./config/passport');
const session = require('express-session'); 
const jwt = require('jsonwebtoken');

const Usuario = require('./models/usuarioModel');
const Token = require('./models/tokenModel');

// Para evitar que salte el mensaje .(node:24754) MaxListenersExceededWarning: Possible EventEmitter memory leak detected. 11 error listeners added to [NativeConnection]. Use emitter.setMaxListeners() to increase limit
var events = require('events');
events.EventEmitter.defaultMaxListeners = 15;

const store = new session.MemoryStore;

var app = express();

app.set('secretKey', 'jwt_pwd_!!223344');

app.use(session({
  cookie: { maxAge: 240 * 60 * 60 * 1000 },
  store: store,
  saveUninitialized: true,
  resave: 'true',
  secret: 'red_bicis!!!***!*.!*.!*.!*.!*.!*.123123'
}));

// including mongoose
var mongoose = require('mongoose');
var mongoDB = 'mongodb://localhost/red_bicicletas';
mongoose.connect(mongoDB, {useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true});

mongoose.Promise = global.Promise;

var db = mongoose.connection;
db.on('error', console.error.bind(console, 'MongoDB Connection error: '));
db.once('open', function() {
  console.log('Conexión exitosa a la base de datos red_bicicletas.');
});

// view engine setup
app.set('views', path.join(__dirname, 'views')); 
app.set('view engine', 'pug');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(passport.initialize());
app.use(passport.session());

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', loggedIn, usersRouter);
app.use('/bicicletas', loggedIn, bicicletasRouter);
app.use('/api/auth', authApiRouter);
app.use('/api/bicicletas', validarUsuario, bicicletasApiRouter);
app.use('/api/usuarios', usuariosApiRouter);
app.use('/usuarios', loggedIn, usuariosRouter);
app.use('/token', tokenRouter);

// Session route
app.get('/login', function(req, res) {
  res.render('session/login', { title: 'Bicicletas' });
});

app.post('/login', function(req, res, next) {
  passport.authenticate('local', function(err, usuario, info) {
    if (err) return next(err);
    if (!usuario) return res.render('session/login', { info, title: 'Bicicletas' });
    req.logIn(usuario, function(err) {
      if (err) return next(err);
      return res.redirect('/');
    });
  })(req, res, next);
});

app.get('/logout', function(req, res) {
  req.logOut();
  res.redirect('/');
});

app.get('/forgot-password', function(req, res) {
  res.render('session/forgot-password', { title: 'Bicicletas' });
});

app.post('/forgot-password', function(req, res) {
  Usuario.findOne({ email: req.body.email }, function(err, usuario) {
    if (!usuario) return res.render('session/forgot-password', { info: {message: 'Email indicado no se encuentra registrado en nuestra base de datos'}, title: 'Bicicletas'});

    usuario.reset_password(function(err) {
      if (err) {
        return next(err);
        console.loog('session/forgot-password-message');
      }

      res.render('session/forgot-password-message', {title: 'Bicicletas'});
    });
  });
});

app.get('/reset-password/:token', function(req, res, next) {
  Token.findOne({ token: req.params.token }, function(err, token) {
    if (!token) return res.status(400).send({ type: 'not-verified', msg: 'No existe un usuario sociado al token. Verifque que su token no haya expirado.' });

    Usuario.findById(token._userId, function(err, usuario) {
      if (!usuario) return res.status(400).send({ msg: 'No existe un usuario asociado al token.'});
      
      res.render('session/reset-password', {errors: {}, usuario, title: 'Bicicletas'});
    });
  });
});

app.post('/reset-password', function(req, res) {
  if (req.body.password != req.body.confirm_password) {
    res.render('session/reset-password', {errors: {confirm_password: {message: 'Los passwords no coinciden'}}, usuario: new Usuario({email: req.body.email}), title: 'Bicicletas'});
    return;
  }
  Usuario.findOne({email: req.body.email}, function(err, usuario) {
    usuario.password = req.body.password;
    usuario.save(function(err) {
      if (err) {
        res.render('session/reset-password', {errors: err.errors, usuario: new Usuario({email: req.body.email}), title: 'Bicicletas'});
      } else {
        res.redirect('/login');
      }
    });
  });
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

function loggedIn(req, res, next) {
  if (req.user) {
    next();
  } else {
    console.log('Acceso no autorizado');
    res.redirect('/login')
  }
};

function validarUsuario(req, res, next) {
  jwt.verify(req.headers['x-access-token'], req.app.get('secretKey'), function(err, decoded) {
    if (err) {
      res.json({status: "error", message: err.message, data: null});
    } else{
      req.body.userId = decoded.id;

      console.log('jwt verify: ' + decoded);
      next();
    }
  });
}


module.exports = app;

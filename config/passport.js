const passport = require('passport');
const localStrategy = require('passport-local').Strategy;

const Usuario = require('../models/usuarioModel');

passport.use(new localStrategy(
  function(email, password, done) {
    Usuario.findOne({email: email}, function(err, usuario) {
      if (err) return done(err);
      if (!usuario) return done(null, false, { message: 'Email no existente' });
      if (!usuario.validPassword(password)) return done(null, false, { message: 'Password es incorrecto' });
      if (!usuario.verificado) return done(null, false, { message: 'La cuenta de usuario no esta verificada' });
      
      return done(null, usuario, null);
    });
  }
));

passport.serializeUser(function(user, cb) {
  cb(null, user.id);
});

passport.deserializeUser(function(id, cb) {
  Usuario.findById(id, function(err, usuario) {
    cb(err, usuario);
  });
});

module.exports = passport;
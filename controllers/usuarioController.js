var Usuario = require('../models/usuarioModel');

module.exports = {
  usuario_list: function(req, res, next) {
    Usuario.allUsers(function(err, usuarios) {
      res.render('usuarios/index', {usuarios, title: 'Bicicletas'});
    });
  },
  usuario_create_get: function(req, res, next) {
    res.render('usuarios/create', {errors: {}, usuario: new Usuario(), title: 'Bicicletas'});
  },
  usuario_create_post: function(req, res, next) {
    if (req.body.password != req.body.confirm_password) {
      res.render('usuarios/create', {errors: {confirm_password: {message: 'Los passwords no coinciden.'}}, title: 'Bicicletas'});
      return;
    }
    var usuario = Usuario.createInstance(req.body.nombre, req.body.email, req.body.password, req.body.confirm_password);
    Usuario.addUser(usuario, function(err, newUser) {
      if (err) {
        res.render('usuarios/create', {errors: err.errors, usuario: new Usuario(), title: 'Bicicletas'});
        return;
      } else {
        newUser.enviar_email_bienvenida();
        res.redirect('/usuarios');
      }
    })
  },
  usuario_update_get: function(req, res, next) {
    Usuario.findUser(req.params.id, function(err, usuario) {
      res.render('usuarios/update', {errors: {}, usuario, title: 'Bicicletas'});
    });
  },
  usuario_update_post: function(req, res, next) {
    var update_values = {nombre: req.body.nombre};
    Usuario.updateUser(req.params.id, update_values, function(err, usuario) {
      if (err) {
        console.log(err);
        res.render('usuarios/update', {errors: err.errors, usuario: new Usuario(), title: 'Bicicletas'});
      } else {
        res.redirect('/usuarios');
        return;
      }
    });
  },
  usuario_delete_post: function(req, res, next) {
    Usuario.deleteUser(req.body.id, function(err) {
      if (err) 
        next(err);
      else 
        res.redirect('/usuarios');
    });
  }
};